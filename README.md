synarere
========

*   Q: Is synarere based on anything?
    
    The concepts and most of the code are from [rkbot][].  I, Michael 
    Rodriguez, took up this project to continue the excellent code and 
    concepts.

[rkbot]: http://drop.malkier.net/rkbot.tar.bz2

*   Q: Any other specific reason you took up this project?

    Yes.  People use mSL (mIRC Scripting Language), and it is a poison.  Some 
    EVEN consider it a programming language. It is not a programming language, 
    it is a scripting language, with an event-based paradigm.  Writing modules 
    in synarere will allow you to obtain more flexibility than mSL, while 
    still writing code with ease.

If you've read this far, congratulations. You are among the few elite people 
that actually read documentation. Thank you.
